SPOX Project (SPectro bOX)
===========


Aim
---
 we want to remotely control the calibration modules for Alpy / Lhires III / LISA spectroscopes.

All these devices are ready for remote control, but there is a need for implementing this remote control.

Harware Design
-----
16/03 : mailing list
- The general architecture of the system is simple :
* A main board of about 70 x 50mm,
* 3 connectors : power supply (12V), USB (type B), and spectro link (Tini XLR connector).
* 2 push buttons,
* 3 LEDs (flat, calibration, dead lamp)
* electronics for power supply, current detection, MOSFET control (for LISA...).
* the arduino Pro mini (5V version), as a sister board
* a box (sheet metal or 3D printing... still to be defined - indeed, I think I'll do both :>).


No wire inside the box, nothing to solder (apart the PCB, of course).

Design
-----
![View1](images/spox_design1.png)
![View2](images/spox_design2.png)
![View3](images/spox_design3.png)

