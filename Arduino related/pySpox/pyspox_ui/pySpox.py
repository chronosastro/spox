# -*- coding: utf-8 -*-

licence = {}
licence['en'] = """
    pySPOX version %s:

    a program to command ardhuino SPOX project
    
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from PySide import QtCore, QtGui
import sys, serial, time, glob
from mainwindow import Ui_MainWindow

# création précoce de l'objet application, déjà nécessaire pour traiter les bugs

class receivePort(QtCore.QThread):
    message = QtCore.Signal(str)

    def __init__(self):
        self.connected = False
        QtCore.QThread.__init__(self)

    def run(self):
        while self.connected:
            try:
                text = self.serial_port.read(1)
                if text != '':
                    self.message.emit(text)
            except serial.SerialException, e:
                self.connected = False


class EmitSerial(QtCore.QThread):
    def __init__(self):
        QtCore.QThread.__init__(self)
        self.connected = False
        self.command = ''

    def run(self):
        try:
            self.serial_port.write(self.command)
            self.exit()
        except serial.SerialException, e:
            self.connected = False


class ControlMainWindow(QtGui.QMainWindow):
    def __init__(self, parent=None):
        super(ControlMainWindow, self).__init__(parent)

        # setup the UI
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.ui.spinBoxDark.setValue(10)
        self.ui.spinBoxCalib.setValue(10)
        self.ui.spinBoxFlat.setValue(10)
        self.ui.pushButtonActualize.setGeometry(0, 0, 22, 22)
        self.ui.pushButtonActualize.setFixedSize(22, 22)
        self.ui.pushButtonActualize.setIcon(QtGui.QIcon("actualize.png"))
        self.ui.radioButtonDark.setCheckable(1)
        self.ui.radioButtonCalib.setCheckable(1)
        self.ui.radioButtonFlat.setCheckable(1)

        self.connected = False
        self.initPorts()
        self.command = ''

        self.listeCommand = ["None", "Calib", "Flat", "Dark"]
        QtCore.QObject.connect(self.ui.pushButtonActualize, QtCore.SIGNAL("clicked()"), self.initPorts)
        QtCore.QObject.connect(self.ui.pushButtonReset, QtCore.SIGNAL("clicked()"), self.resetLamps)
        QtCore.QObject.connect(self.ui.pushButtonCalib, QtCore.SIGNAL("clicked()"), self.takeCalib)
        QtCore.QObject.connect(self.ui.pushButtonFlat, QtCore.SIGNAL("clicked()"), self.takeFlat)
        QtCore.QObject.connect(self.ui.pushButtonDark, QtCore.SIGNAL("clicked()"), self.takeDark)
        QtCore.QObject.connect(self.ui.pushButtonConnect, QtCore.SIGNAL("clicked()"), self.connectSerial)

        # init Timer polling serial
        self.timerSerial = QtCore.QTimer()
        self.connect(self.timerSerial, QtCore.SIGNAL('timeout()'), self.pollSerial)

        self.timerSerial.start(4556)

        self.debug = False
        self.roll = True  # boolean counting polling on all interrupts.

    def initPorts(self):
        self.ui.comboBoxConnect.clear()
        for port in self.serial_ports():
            self.ui.comboBoxConnect.addItem(port)

    def writeSerial(self, command):
        self.writeThread = EmitSerial()
        self.writeThread.serial_port = self.serialThread.serial_port
        self.writeThread.connected = True
        self.writeThread.command = command
        self.writeThread.start()
        while self.writeThread.isRunning():
            time.sleep(0.01)
            continue

    def takeCalib(self):
        if self.connected:
            try:
                self.timer.stop()
            except:
                pass
            self.writeSerial("11\n")
            time.sleep(0.25)

            #twice to avoid serial issues
            self.writeSerial("11\n")
            time.sleep(0.25)

            self.counter = self.ui.spinBoxCalib
            self.setTimer()
            self.ui.textBrowserInfos.insertHtml("Calib lamps are On for " + str(self.counter.value()) + " seconds<br>")
        else:
            self.ui.textBrowserInfos.insertHtml("Not connected<br>")

    def takeFlat(self):
        if self.connected:
            try:
                self.timer.stop()
            except:
                pass
            self.writeSerial("21\n")
            time.sleep(0.25)
            #twice to avoid serial issues
            self.writeSerial("21\n")
            time.sleep(0.25)
            self.counter = self.ui.spinBoxFlat
            self.setTimer()
            self.ui.textBrowserInfos.insertHtml("Flat lamps are On for " + str(self.counter.value()) + " seconds<br>")


        else:
            self.ui.textBrowserInfos.insertHtml("Not connected<br>")

    def takeDark(self):
        if self.connected:
            try:
                self.timer.stop()
            except:
                pass
            self.writeSerial("31\n")
            time.sleep(0.25)

            # send twice to avoid serial issue
            self.writeSerial("31\n")
            time.sleep(0.25)

            self.counter = self.ui.spinBoxDark
            self.setTimer()
            self.ui.textBrowserInfos.insertHtml("Ready for Darks for " + str(self.counter.value()) + " seconds<br>")

        else:
            self.ui.textBrowserInfos.insertHtml("Not connected<br>")

    def setTimer(self):
        self.timer = QtCore.QTimer()
        self.connect(self.timer, QtCore.SIGNAL('timeout()'), self.reverseCount)
        self.timer.start(1000)

    def reverseCount(self):
        print('OK', self.counter.value()-1)
        self.counter.setValue(self.counter.value()-1)
        if self.counter.value()==0:
            self.resetLamps()


    def resetLamps(self):
        if self.connected:
            print ("enter in resetLamps")
            time.sleep(0.1)
            self.writeSerial("10\n")
            time.sleep(0.1)
            self.writeSerial("20\n")
            time.sleep(0.1)

            # Send value twice. (to avoid serial errors)

            time.sleep(0.1)
            self.writeSerial("10\n")
            time.sleep(0.1)
            self.writeSerial("20\n")
            time.sleep(0.1)

            try:
                self.timer.stop()
                self.ui.textBrowserInfos.insertHtml("Last Command Stopped<br>")

            except AttributeError:
                print('ttt')  # no timer defined
        else:
            self.ui.textBrowserInfos.insertHtml("Not connected<br>")

    def connectSerial(self):
        # port = self.ui.comboBoxConnect.itemText(self.ui.comboBoxConnect.currentIndex())
        # try:
        # self.ser = serial.Serial(port, 9600)
        # self.connected = True
        #     self.ui.textBrowserInfos.insertHtml("<b>Arduino connected</b><br>")
        # except OSError:
        #     self.ui.textBrowserInfos.insertHtml("<b>Bad Serial Adress</b><br>")

        try:
            self.serialThread = receivePort()
            self.serialThread.message.connect(self.treatSerial, QtCore.Qt.QueuedConnection)
            self.serialThread.serial_port = serial.Serial(
                self.ui.comboBoxConnect.itemText(self.ui.comboBoxConnect.currentIndex()), 9600, timeout=None)
            self.connected = True
            self.ui.textBrowserInfos.insertHtml("<b>Arduino connected</b><br>")
            self.serialThread.connected = True
            self.serialThread.start()
        except OSError:
            self.ui.textBrowserInfos.insertHtml("<b>Bad Serial Adress</b><br>")

    def treatSerial(self, text):


        if text == '\n':
            text = '<br>'
            if self.roll:
                self.stateCalib = False
                self.stateFlat = False
                self.stateDark = False
                self.roll = False

            self.command += text

            # print(['command ', self.stateCalib, self.stateFlat, self.command,self.command[0], self.command[1] ])
            if self.debug:
                if 'USBRC' in self.command:
                    self.ui.textBrowserInfos.insertHtml("<b>Serial Error</b>")
                    self.ui.textBrowserInfos.insertHtml(
                        '<br>'.join(['command ', str(self.stateCalib), str(self.stateFlat), self.command]))
            if str(self.command[0]) == '1':
                if self.command[1] == '1':
                    self.stateCalib = True
            if str(self.command[0]) == '2':
                if self.command[1] == '1':
                    self.stateFlat = True
            if str(self.command[0]) == '3' or (self.stateFlat and self.stateCalib):
                self.stateDark = True

            print(self.stateCalib, self.stateFlat, self.stateDark)


            if (self.stateDark):
                self.ui.radioButtonDark.setChecked(1)
                self.ui.radioButtonFlat.setChecked(0)
                self.ui.radioButtonCalib.setChecked(0)
            else:
                self.ui.radioButtonDark.setChecked(0)

            if (self.stateFlat and (not self.stateDark)):
                self.ui.radioButtonFlat.setChecked(1)
            else:
                self.ui.radioButtonFlat.setChecked(0)
            if (self.stateCalib and (not self.stateDark)):
                self.ui.radioButtonCalib.setChecked(1)
            else:
                self.ui.radioButtonCalib.setChecked(0)

            self.updateScroll()
            self.command = ''

        else:
            self.command += text

    def closeEvent(self, event):
        # shut off lamps
        self.resetLamps()
        time.sleep(0.1)

        # finish threads
        self.writeThread.exit()
        self.serialThread.exit()


    def serial_ports(self):
        """Lists serial ports

        :raises EnvironmentError:
            On unsupported or unknown platforms
        :returns:
            A list of available serial ports
        """
        if sys.platform.startswith('win'):
            ports = ['COM' + str(i + 1) for i in range(256)]

        elif sys.platform.startswith('linux') or sys.platform.startswith('cygwin'):
            # this is to exclude your current terminal "/dev/tty"
            ports = glob.glob('/dev/tty[A-Za-z]*')

        elif sys.platform.startswith('darwin'):
            ports = glob.glob('/dev/tty.*')

        else:
            raise EnvironmentError('Unsupported platform')

        result = []
        for port in ports:
            try:
                s = serial.Serial(port)
                s.close()
                result.append(port)
            except (OSError, serial.SerialException):
                pass
        return result

    def pollSerial(self):
        if self.connected:
            self.writeSerial("1?\n")
            time.sleep(0.05)

            self.writeSerial("2?\n")
            time.sleep(0.05)

            self.roll = True

    def updateScroll(self):
        self.ui.textBrowserInfos.verticalScrollBar().setValue(self.ui.textBrowserInfos.verticalScrollBar().maximum())


if __name__ == "__main__":
    app = QtGui.QApplication(sys.argv)
    mySW = ControlMainWindow()
    mySW.show()
    sys.exit(app.exec_())

